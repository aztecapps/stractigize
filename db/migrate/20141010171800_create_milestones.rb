class CreateMilestones < ActiveRecord::Migration
  def change
    create_table :milestones do |t|
      t.string :name
      t.text :description
      t.integer :project_id
      t.integer :user_id
      t.datetime :due_date
      t.integer :status
      t.text :notes
      t.datetime :status_last_updated
      t.datetime :notes_last_updated

      t.timestamps
    end
  end
end
