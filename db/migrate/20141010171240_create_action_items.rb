class CreateActionItems < ActiveRecord::Migration
  def change
    create_table :action_items do |t|
      t.string :short_description
      t.text :details
      t.string :action_type
      t.integer :process_id
      t.integer :project_id
      t.integer :milestone_id
      t.integer :user_id
      t.datetime :due_date
      t.integer :status
      t.text :notes
      t.integer :client_id
      t.integer :entered_by
      t.datetime :status_last_updated
      t.datetime :notes_last_updated

      t.timestamps
    end
  end
end
