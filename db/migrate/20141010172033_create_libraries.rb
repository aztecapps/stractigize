class CreateLibraries < ActiveRecord::Migration
  def change
    create_table :libraries do |t|
      t.string :document_name
      t.text :description
      t.string :document_type
      t.integer :topic_id
      t.integer :category_id
      t.integer :client_id
      t.integer :user_id

      t.timestamps
    end
  end
end
