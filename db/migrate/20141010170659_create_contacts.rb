class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :first_name
      t.string :last_name
      t.string :primary_phone
      t.string :secondary_phone
      t.string :email
      t.string :address
      t.string :address_2
      t.string :city
      t.string :state
      t.string :zip
      t.integer :pc_team_member
      t.integer :supervisor_id
      t.integer :primary_contact
      t.integer :client_id
      t.integer :user_id

      t.timestamps
    end
  end
end
