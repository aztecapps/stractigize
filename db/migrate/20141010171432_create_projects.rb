class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.integer :client_id
      t.integer :user_id
      t.integer :status
      t.integer :system_status
      t.integer :created_by

      t.timestamps
    end
  end
end
