class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.date :start_date
      t.integer :user_id
      t.text :notes

      t.timestamps
    end
  end
end
