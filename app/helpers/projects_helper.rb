module ProjectsHelper

  def project_statuses
    [
      ['Not Started', 'Not Started'],
      ['On Track', 'On Track'],
      ['At Risk', 'At Risk'],
      ['Off Track', 'Off Track'],
      ['Blocked', 'Blocked']
    ]
  end

  def system_statuses
    [
      ['Active', 'Active'],
      ['Inactive', 'Inactive'],
      ['Complete', 'Complete'],
      ['Hold', 'Hold']
    ]
  end

end
