module MilestonesHelper

  def milestone_statuses
    [
      ['Not Started', 'Not Started'],
      ['On Track', 'On Track'],
      ['At Risk', 'At Risk'],
      ['Off Track', 'Off Track'],
      ['Hold', 'Hold'],
      ['Blocked', 'Blocked']
    ]
  end

end
