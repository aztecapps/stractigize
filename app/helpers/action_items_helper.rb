module ActionItemsHelper

  def action_types
    [
      ['Action Item', 'Action Item'],
      ['Project Task', 'Project Task'],
      ['Project Milestone', 'Project Milestone'],
      ['Risk', 'Risk'],
      ['Process Task', 'Process Task']
    ]
  end

  def action_statuses
    [
      ['Not Started', 'Not Started'],
      ['In Progress', 'In Progress'],
      ['Hold', 'Hold'],
      ['Blocked', 'Blocked'],
      ['Completed', 'Completed'],
      ['Closed', 'Closed']
    ]
  end

end
