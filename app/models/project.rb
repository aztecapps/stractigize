class Project < ActiveRecord::Base

  belongs_to :client, inverse_of: :projects
  belongs_to :user, inverse_of: :projects
  belongs_to :creator, foreign_key: :created_by, class_name: 'User'

  has_many :milestones, inverse_of: :project
  has_many :action_items, inverse_of: :project

  validates :client, presence: true
  validates :user, presence: true

end
