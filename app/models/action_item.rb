class ActionItem < ActiveRecord::Base

  belongs_to :client, inverse_of: :action_items
  belongs_to :project, inverse_of: :action_items
  belongs_to :milestone, inverse_of: :action_items
  belongs_to :user, inverse_of: :action_items

  validates :user, presence: true
  validates :milestone, presence: true
  validates :project, presence: true
  validates :client, presence: true

end
