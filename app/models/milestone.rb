class Milestone < ActiveRecord::Base

  belongs_to :project, inverse_of: :milestones
  belongs_to :user, inverse_of: :milestones

  has_many :action_items, inverse_of: :milestone

  validates :project, presence: true
  validates :user, presence: true

end
