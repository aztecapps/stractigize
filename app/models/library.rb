class Library < ActiveRecord::Base

  belongs_to :client, inverse_of: :libraries
  belongs_to :user, inverse_of: :libraries

  validates :client, presence: true
  validates :user, presence: true

end
