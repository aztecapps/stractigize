class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :clients, inverse_of: :user
  has_many :projects, inverse_of: :user
  has_many :milestones, inverse_of: :user
  has_many :action_items, inverse_of: :user
  has_many :contacts, inverse_of: :contact
  has_many :libraries, inverse_of: :user

  def full_name
    "#{first_name} #{last_name}"
  end

end
