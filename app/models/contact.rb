class Contact < ActiveRecord::Base

  belongs_to :client, inverse_of: :contacts
  belongs_to :user, inverse_of: :contacts
  belongs_to :supervisor, foreign_key: :supervisor_id, class_name: 'User'

  validates :client, presence: true
  validates :user, presence: true
  validates :supervisor, presence: true

end
