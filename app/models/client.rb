class Client < ActiveRecord::Base

  has_many :libraries, inverse_of: :client
  has_many :projects, inverse_of: :client
  has_many :contacts, inverse_of: :client
  has_many :action_items, inverse_of: :client

  belongs_to :user, inverse_of: :clients

  validates :user, presence: true
  
end
