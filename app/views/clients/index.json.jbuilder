json.array!(@clients) do |client|
  json.extract! client, :id, :name, :start_date, :user_id, :notes
  json.url client_url(client, format: :json)
end
