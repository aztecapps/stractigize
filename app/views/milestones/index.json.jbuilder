json.array!(@milestones) do |milestone|
  json.extract! milestone, :id, :name, :description, :project_id, :user_id, :due_date, :status, :notes, :status_last_updated, :notes_last_updated
  json.url milestone_url(milestone, format: :json)
end
