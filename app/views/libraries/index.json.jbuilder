json.array!(@libraries) do |library|
  json.extract! library, :id, :document_name, :description, :document_type, :topic_id, :category_id, :client_id, :user_id
  json.url library_url(library, format: :json)
end
