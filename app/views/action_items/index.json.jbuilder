json.array!(@action_items) do |action_item|
  json.extract! action_item, :id, :short_description, :details, :action_type, :process_id, :project_id, :milestone_id, :user_id, :due_date, :status, :notes, :client_id, :entered_by, :status_last_updated, :notes_last_updated
  json.url action_item_url(action_item, format: :json)
end
