json.array!(@projects) do |project|
  json.extract! project, :id, :name, :description, :client_id, :user_id, :status, :system_status, :created_by
  json.url project_url(project, format: :json)
end
